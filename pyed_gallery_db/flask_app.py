from flask import Flask, render_template, request, url_for, redirect

app = Flask(__name__)
app.config.from_json('config.json')


@app.route('/')
def index():
    data = {
        'galleries_list': [
            {'name': 'gallery 1'},
            {'name': 'gallery 2'}
        ],
        'gallery': {
            'name': 'abc',
            'children': [
                {
                    'name': 'Wonka 1',
                    'url': 'http://files2.ostagram.ru/uploads/style/image/89773/thumb_img_0107385e40.jpg'
                },
                {
                    'name': 'Wonka 2',
                    'url': 'http://files2.ostagram.ru/uploads/style/image/89773/thumb_img_0107385e40.jpg'
                }
            ]
        },
    }
    galleries_list = data['galleries_list']
    gallery_name = data['gallery']['name']
    gallery_children = data['gallery']['children']
    return render_template('gallery-view.html',
                           galleries_list=galleries_list,
                           gallery_name=gallery_name,
                           children=gallery_children)


@app.route("/upload_page")
def upload_page():
    return render_template("upload-content.html")


@app.route("/upload", methods=['POST'])
def upload():
    galleryname = request.args.getlist('galleryname')
    uploaded_files = request.files.getlist('file[]')
    print galleryname
    print uploaded_files
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run('0.0.0.0', debug = True)
